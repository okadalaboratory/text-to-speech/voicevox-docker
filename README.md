# VoiceVox ROS Noetic をDockerで動かす
[VOICEVOX | 無料のテキスト読み上げソフトウェア](https://voicevox.hiroshiba.jp/)

- 商用・非商用問わず無料 <BR>
- すぐに使えるソフトウェア <BR>
Windows, Mac, Ubuntu, Python API
- イントネーションの詳細な調整が可能<br>

ここではDockerを使い、ROSから簡単にVoiceVoxを使ってみます。

音声の出力方法には下記の２バージョンがあります。
- 音声合成ノードの起動(音声の再生は Python-sounddevice を使う)<br>
 Python-sounddevice を使い、ROSノードを起動したPCのスピーカーから音声を再生する。

- 音声合成ノードの起動(音声の再生は audio_play ノードを使う)<br>
発話の初めに「ブツっ」というノイズが入る（未解決）。
![](https://gitlab.com/okadalaboratory/audio/text-to-speech/voicevox-docker/-/raw/images/voicevox.png?ref_type=heads)

## To Do
31 Dec. 2023
大幅改修中

3 Jun. 2023<br>
CPU版だけにした。

15 Apr. 2023<br>
一気に起動(工事中）

23 Feb. 2023<br>
ユーザ辞書の利用

23 Feb. 2023<br>
voicevox_core0.14.1から0.14.2にバージョンアップ

23 Feb. 2023<br>
~~- 実行時のDocker専用のユーザを作る~~<br>
~~- VoiceVoxは日本語だけに対応しているので、日本語環境にする~~<br>
~~- パラメータの変更（話者ほか）~~
    - speaker_id 話者ID
    - use_gpu GPUを使う
    - f0_speaker_id 音高の話者ID（デフォルト値はspeaker_id）
    - f0_correct 音高の補正値（デフォルト値は0。+-0.3くらいで結果が大きく変わります）

19 Feb. 2023<br>
~~Docker内で音声の入出力は不安定なので、ホスト側で行う~~<br>
Dockerコンテナで音声の入出力を実装済み


## Dockerコンテナで音声を再生する準備
Dockerコンテナ側では音を再生できないので、以下の手段でDockerコンテナ側から生成された音声をホストPCで再生します。
Docker コンテナで音声を再生するために、ホストコンピュータのPulseaudioを使います。

### /tmp/pulseaudio.socketを生成
下記のコマンドでホストPCの/tmp/pulseaudio.socketを生成する。同じ名前のディレクトリが存在する場合は、そのディレクトリを削除します。

```
ホストPC $ ls -al 
合計 72
drwxrwxr-x 18 roboworks roboworks 4096 12月 29 15:44 .
drwxr-x--- 29 roboworks roboworks 4096 12月 29 10:33 ..
drwxrwxr-x  4 roboworks roboworks 4096 12月 25 10:53 pulseaudio.socket
...
...
ホストPC $ sudo rm -r /tmp/pulseaudio.socket
ホストPC $ pacmd load-module module-native-protocol-unix socket=/tmp/pulseaudio.socket
```

### /etc/pulse/default.pa　の編集
ホストコンピュータの /etc/pulse/default.pa　に、下記 socket=...の記述を追加します。
```
load-module module-native-protocol-unix
load-module module-native-protocol-unix socket=/tmp/pulseaudio.socket
```

### 再起動
再起動してください。

## インストール
```
cd ~
git clone https://gitlab.com/okadalaboratory/text-to-speech/voicevox-noetic-docker.git
```

## Dockerイメージの作成
開発環境のDockerイメージはCPU版が用意されています。
```
cd ~/voicevox-noetic-docker
docker compose build
```

## Dockerコンテナの起動
```
docker compose up -d
[+] Running 2/0
 ✔ Container voicevox                      Created                               0.0s 
 ✔ Container voicevox-docker-ros-master-1  Created                               0.0s 
Attaching to voicevox, voicevox-docker-ros-master-1
voicevox                      | voicevox@voicevox:~$ roslaunch voicevox_ros voicevox_sounddevice.launch speaker:=1
...
...
voicevox                      | NODES
voicevox                      |   /
voicevox                      |     voicevox (voicevox_ros/voicevox_sounddevice.py)
voicevox                      | 
voicevox                      | ROS_MASTER_URI=http://ros-master:11311
voicevox                      | 
voicevox                      | process[voicevox-1]: started with pid [61]
voicevox                      | [INFO] [1703641807.186262]: say opening Message
```
コンテナ起動のオープニングメッセージが流れれば成功です。


## テキストから音声の合成
ROS NoeticのインストールされているPCから音声合成を試します。
```
$ rostopic list
/rosout
/rosout_agg
/voicevox/request
```
下記の通り、/voicevox/requestノードに文字列を送ることで、音声が合成されます。
```
rostopic pub /voicevox/request std_msgs/String "data: '皆さんこんにちは'"  
```
スピーカーから音声が再生されます。

## 話者の変更
### 起動時のパラメータで指定します。
docker-compose.ymlを修正してください。
```
roslaunch voicevox_ros voicevox.launch speaker:=10
```

### ROSパラメータを変更することで話者を切り替えます
```
$ rosparam list 
/rosdistro
/roslaunch/uris/host_flow_z13__34517
/roslaunch/uris/host_flow_z13__41811
/rosversion
/run_id
/voicevox/speaker_id
```

下記のように、話者を6に変更できます。
```
$ rosparam set /voicevox/speaker_id 6
```

## 試してみる
```
cd ~/voicevox-docker
$ docker compose exec ros-master /bin/bash
root@ros-master:/# source /opt/ros/noetic/setup.bash
root@ros-master:/# rostopic pub -1 /voicevox/request std_msgs/String "data: 'これはテストです'"
publishing and latching message for 3.0 seconds
root@ros-master:/# rosparam set /voicevox/speaker_id 6
root@ros-master:/# rostopic pub -1 /voicevox/request std_msgs/String "data: '話者を切り替えました'"
publishing and latching message for 3.0 seconds
root@ros-master:/# 
```

## 付録
[話者一覧と話者ID](https://ponkichi.blog/raspberry-voicevox/#st-toc-h-7)
| 話者  |  話者ID  |
| ---- | ---- |
|  四国めたん：ノーマル  |  2  |
|  四国めたん：あまあま  |  0 |
|  四国めたん：ツンツン  |  6 |
|  四国めたん：セクシー  |  4 |
| ---- | ---- |
|  ずんだもん：ノーマル  |  3  |
|  ずんだもん：あまあま  |  1 |
|  ずんだもん：ツンツン  |  7 |
|  ずんだもん：セクシー  |  5 |
| ---- | ---- |
|  春日部つむぎ：ノーマル  |  8  |
| ---- | ---- |
|  雨晴はう：ノーマル  |  10  |
| ---- | ---- |
|  波音リツ：ノーマル  |  9  |
| ---- | ---- |
|  玄野武宏：ノーマル  |  11  |
| ---- | ---- |
|  白上虎太郎：ノーマル  |  12  |
| ---- | ---- |
|  青山龍星：ノーマル  |  13  |
| ---- | ---- |
|  冥鳴ひまり：ノーマル  |  14  |
| ---- | ---- |
|  九州そら：ノーマル  |  16  |
|  九州そら：あまあま  |  15 |
|  九州そら：ツンツン  |  18 |
|  九州そら：セクシー  |  17 |
|  九州そら：ささやき  |  19 |
| ---- | ---- |
|  もち子さん：ノーマル  |  20 |
|  剣崎雌雄：ノーマル  |  21 |
|  WhiteCUL：ノーマル  |  23 |
|  WhiteCUL：たのしい  |  24 |
|  WhiteCUL：かなしい  |  25 |
|  WhiteCUL：びえーん  |  26 |
|  後鬼	人間ver.  |  27 |
|  ぬいぐるみver.  |  28 |
|  No.7:ノーマル  |  29 |
|  アナウンス  |  30 |
|  読み聞かせ  |  31 |
|  ちび式じい:ノーマル  |  42 |
|  櫻歌ミコ：ノーマル  |  43 |
|  第二形態  |  44 |
|  ロリ  |  45 |
|  小夜/SAYO:ノーマル  |  45 |
|  ナースロボ＿タイプＴ:ノーマル  |  47 |
|  楽々  |  48 |
|  恐怖  |  49 |
|  内緒話 |  50 |




## VoiceVoxについて
[WEB版VOICEVOX(非公式)](https://voicevox.su-shiki.com/)

### ライセンス
#### OSS版と製品版
OSS版は製品版からキャラクターを除いたもの

> キャラクターは多くの場合 OSS と相反するライセンスを持っているため、OSS にすることができません。 
> 例えば、キャラクターのライセンスによくある「公序良俗に反する利用の禁止」や「反社会的勢力の排除」は、
> オープンソースの定義の１つである「利用する分野に対する差別の禁止」と衝突します。

### VOICE VOXの構成
#### エディター
- GUI を表示するためのモジュールで、アプリケーションの形で提供
- リポジトリは [voicevox](https://github.com/VOICEVOX/voicevox) 

#### エンジン
- テキスト音声合成 API を公開するためのモジュールで、Web サーバーの形で提供
- 実態は HTTP サーバーなので、リクエストを送信すればテキスト音声合成ができる
- リポジトリは [voicevox_engine](https://github.com/VOICEVOX/voicevox_engine)

#### コア
- 音声合成に必要な計算を実行するためのモジュールで、動的ライブラリの形で提供
- リポジトリは [voicevox_core](https://github.com/VOICEVOX/voicevox_core)



# ダメな時

[Dockerコンテナ上のGUIアプリで音を出す（Linux）](https://zenn.dev/ysuito/articles/971c524f919cd0)

## メモ
```
pacmd load-module module-native-protocol-unix socket=/tmp/pulseaudio.socket;docker run --rm -it --privileged --net=host --ipc=host --device=/dev/snd:/dev/snd --env PULSE_SERVER=unix:/tmp/pulseaudio.socket --env PULSE_COOKIE=/tmp/pulseaudio.cookie --volume /tmp/pulseaudio.socket:/tmp/pulseaudio.socket okdhryk/voicevox-noetic:cpu roslaunch voicevox_ros voicevox.launch speaker:=10
```