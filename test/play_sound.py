#!/usr/bin/env python3
# Copyright (c) 2023 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
## coding: UTF-8
import os
import simpleaudio as sa
from io import BytesIO
import sounddevice as sd
import soundfile as sf
import numpy as np
import ctypes
ctypes.cdll.LoadLibrary('/voicevox/onnxruntime-linux-x64-1.13.1/lib/libonnxruntime.so')

from pathlib import Path
from voicevox_core import VoicevoxCore, METAS

core = VoicevoxCore(open_jtalk_dict_dir=Path("/voicevox/open_jtalk_dic_utf_8-1.11"))
speaker_id = 2
text = "こんにちは、これはテストです。"
if not core.is_model_loaded(speaker_id):  # モデルが読み込まれていない場合
    core.load_model(speaker_id)  # 指定したidのモデルを読み込む

wave_bytes = core.tts(text, speaker_id)  # 音声合成を行う
wav_stream = BytesIO(wave_bytes)
audio_array, sampling_rate = sf.read(wav_stream)
sd.play(audio_array, sampling_rate)
sd.wait() # sd.playが完了するのを待つ
speaker_id = 4
text = "こんにちは、これは2度目のテストです。"
if not core.is_model_loaded(speaker_id):  # モデルが読み込まれていない場合
    core.load_model(speaker_id)  # 指定したidのモデルを読み込む

wave_bytes = core.tts(text, speaker_id)  # 音声合成を行う
wav_stream = BytesIO(wave_bytes)
audio_array, sampling_rate = sf.read(wav_stream)
sd.play(audio_array, sampling_rate)
sd.wait() # sd.playが完了するのを待つ
