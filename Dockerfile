# Copyright (c) 2023 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php
FROM osrf/ros:noetic-desktop-full-focal

LABEL maintainer="Hiroyuki Okada <hiroyuki.okada@okadanet.org>"
LABEL org.okadanet.vendor="Hiroyuki Okada" \
      org.okadanet.dept="TRCP" \
      org.okadanet.version="1.0.0" \
      org.okadanet.released="December 31, 2023"

SHELL ["/bin/bash", "-c"]
ENV DEBIAN_FRONTEND noninteractive 

# Timezone, Launguage設定
RUN apt update \
  && apt install -y --no-install-recommends \
     locales \
     language-pack-ja-base language-pack-ja \
     software-properties-common tzdata \
     fonts-ipafont fonts-ipaexfont fonts-takao
RUN  locale-gen ja_JP ja_JP.UTF-8  \
  && update-locale LC_ALL=ja_JP.UTF-8 LANG=ja_JP.UTF-8 \
  && add-apt-repository universe
# Locale
ENV LANG ja_JP.UTF-8
ENV TZ=Asia/Tokyo

RUN apt-get update && apt-get install -y \
    wget build-essential gcc git vim \
    lsb-release iproute2 gnupg gnupg2 gnupg1 \
    ca-certificates language-pack-ja-base  \
    language-pack-ja locales fonts-takao \
    python3-pip \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /voicevox
RUN wget https://github.com/VOICEVOX/voicevox_core/releases/download/0.14.3/voicevox_core-0.14.3+cpu-cp38-abi3-linux_x86_64.whl
RUN pip3 install voicevox_core-0.14.3+cpu-cp38-abi3-linux_x86_64.whl

RUN wget https://github.com/microsoft/onnxruntime/releases/download/v1.13.1/onnxruntime-linux-x64-1.13.1.tgz
RUN tar -xzvf onnxruntime-linux-x64-1.13.1.tgz

RUN wget http://sourceforge.net/projects/open-jtalk/files/Dictionary/open_jtalk_dic-1.11/open_jtalk_dic_utf_8-1.11.tar.gz
RUN tar zxvf open_jtalk_dic_utf_8-1.11.tar.gz

# install audio
RUN apt-get update && apt-get install -y portaudio19-dev && rm -rf /var/lib/apt/lists/*
RUN apt-get update && apt-get install -y  \
ros-noetic-audio-common pulseaudio alsa-utils  ffmpeg   \
python3-soundfile python3-pyaudio mesa-utils \
 && rm -rf /var/lib/apt/lists/*
RUN pip3 install simpleaudio sounddevice soundfile

# Create an overlay Catkin workspace
RUN source /opt/ros/noetic/setup.bash \
 && mkdir -p /overlay_ws/src \
 && cd /overlay_ws/src \ 
 && catkin_init_workspace \ 
 && git clone https://gitlab.com/okadalaboratory/chat/text-to-speech/voicevox_ros.git  \ 
 && cd /overlay_ws \
 && rosdep update \
 && rosdep install --from-paths src --ignore-src -r -y \
 && catkin_make

COPY test/make_sound.py make_sound.py
COPY test/play_sound.py play_sound.py
RUN chmod a+x make_sound.py
RUN chmod a+x play_sound.py

COPY assets/pulseaudio.client.conf /etc/pulse/client.conf
COPY assets/ros_entrypoint.sh /tmp
RUN chmod a+x /tmp/ros_entrypoint.sh

ARG USERNAME=voicevox
ARG GROUPNAME=voicevox
ARG UID=1000
ARG GID=1000
RUN groupadd -g $GID $GROUPNAME && \
    useradd -m -s /bin/bash -u $UID -g $GID $USERNAME
USER $USERNAME
WORKDIR /home/$USERNAME/
RUN echo "source /opt/ros/noetic/setup.bash" >> /home/$USERNAME/.bashrc
RUN echo "source /overlay_ws/devel/setup.bash" >> /home/$USERNAME/.bashrc
RUN rosdep update
ENTRYPOINT [ "/tmp/ros_entrypoint.sh" ]
CMD ["/bin/bash"]
